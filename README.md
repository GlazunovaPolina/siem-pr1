# Практическая работа 1
## Создать 2 виртуальные машины на базе ОС Debian 12
Создаем виртуальные машины и смотрим ip 

![IMAGE_DESCRIPTION](screeen/1.png)


## Обеспечить между ними сетевой обмен
Проверяем доступность машин

![IMAGE_DESCRIPTION](screeen/2.png)

## Включить на 1й из ВМ передачу логов по протоколу rsyslog на 2ю ВМ
Выполняем установку сервиса rsyslog на обе ВМ

![IMAGE_DESCRIPTION](screeen/3.png)

Проверим, что сервис функционирует

![IMAGE_DESCRIPTION](screeen/4.png)

Отредактируем конфигурационные файлы на первой и второй машине

![IMAGE_DESCRIPTION](screeen/5.png)

## Установить и настроить получение логов на сервер с использованием Loki
Установим Loki

![IMAGE_DESCRIPTION](screeen/6.png)

Скачаем конфигурационный файл, архив с утилитой и разархивируем его

![IMAGE_DESCRIPTION](screeen/7.png)

Зупустим Loki

![IMAGE_DESCRIPTION](screeen/8.png)

Проверим работоспособоность Loki через браузер

![IMAGE_DESCRIPTION](screeen/9.png)

На второй машине скачаем конфигурационный файл агента Promtail

![IMAGE_DESCRIPTION](screeen/10.png)

Скачаем архив с утилитой и разархивируем его

![IMAGE_DESCRIPTION](screeen/11.png)

Изменим конфигурационный файл 

![IMAGE_DESCRIPTION](screeen/12.png)

Запустим Promtail

![IMAGE_DESCRIPTION](screeen/13.png)

Проверим работоспособность через браузер

![IMAGE_DESCRIPTION](screeen/14.png)

## Работа с SigNoz
Скачаем необходимые файлы для установки 

![IMAGE_DESCRIPTION](screeen/15.png)

Выполним установку

![IMAGE_DESCRIPTION](screeen/16.png)

Установку выполнить не удалось(

![IMAGE_DESCRIPTION](screeen/17.png)
